class Solution {
    public List<Integer> findDuplicates(int[] nums) {
        HashSet<Integer> hs=new HashSet<>();
        ArrayList<Integer> al=new ArrayList<>();
        for(int i: nums){
            if(!hs.add(i)){
                al.add(i);
            }
        }return al;
        
    }
}
