import java.util.*;
class Solution{
	public long minDiff(ArrayList<Integer> a,int c,int d){
		Collections.sort(a);
		int res=Integer.MAX_VALUE;
		for(int i=0;i<=c-d;i++){
			int min=a.get(i);
			int max=a.get(i+d-1);
			int diff=max-min;
			if(diff<res){
				res=diff;
			}
		}
		return res;
	}
}
class B{
	public static void main(String[] args){
		Solution obj=new Solution();
		ArrayList<Integer>al=new ArrayList<Integer>();
		Scanner sc=new Scanner(System.in);
		int size=sc.nextInt();
		int d = sc.nextInt();
		for(int i=0;i<size;i++){
			int x=sc.nextInt();
			al.add(x);
		}
		long ans=obj.minDiff(al,size,d);
		System.out.println(ans);
	}
}


