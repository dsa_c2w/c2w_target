import java.util.*;
class Solution{
	public int Melement(int[] nums){
		Arrays.sort(nums);
		int length=nums.length;
		int ans=nums[(length/2)];
		return ans;
	}
}
class B{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		int size=sc.nextInt();
		int arr[]=new int[size];
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		Solution obj=new Solution();
		int ret=obj.Melement(arr);
		System.out.println(ret);
	}
}
