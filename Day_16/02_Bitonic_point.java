class Solution {
    int findMaximum(int[] arr, int n) {
        int max_ele=Integer.MIN_VALUE;
        for(int i=0;i<n;i++){
            if(arr[i]>max_ele){
                max_ele=arr[i];
            }
        }return max_ele;
    }
}
