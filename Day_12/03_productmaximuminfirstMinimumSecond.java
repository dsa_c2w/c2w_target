class Solution{
    
    
    public static long find_multiplication (int arr[], int brr[], int n, int m) {
        int product=1;
        int min_ele=Integer.MAX_VALUE;
        int max_ele=Integer.MIN_VALUE;
        for(int i=0;i<arr.length;i++){
            if(arr[i]>max_ele){
                max_ele=arr[i];
            }
        }
        for(int i=0;i<brr.length;i++){
            if(brr[i]<min_ele){
                min_ele=brr[i];
            }
        }
        product=(max_ele*min_ele);
        return product;
    }
    
    
}

