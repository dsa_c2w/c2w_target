class Solution {
    
    public pair indexes(long v[], long x) {
        long first = -1;
        long second = -1;
        
        for (int i = 0; i < v.length; i++) {
            if (v[i] == x) {
                first = i;
                break;
            }
        }
        
        for (int i = v.length - 1; i >= 0; i--) {
            if (v[i] == x) {  // Change from v[i] == i to v[i] == x
                second = i;
                break;
            }
        }
        
        return new pair(first, second);
    }
}

