
class Solution {
    int rowWithMax1s(int arr[][], int n, int m) {
        int index=-1;
        int maxoccurance=0;
        for(int i=0;i<n;i++){
            int count=0;
            for(int j=0;j<m;j++){
                if(arr[i][j]==1){
                    count++;
                }
            }
            if(count>maxoccurance){
                maxoccurance=count;
                index=i;
            }
        }
        return index;
    }
}
