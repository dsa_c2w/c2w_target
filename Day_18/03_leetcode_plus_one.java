class Solution {
    public int[] rowAndMaximumOnes(int[][] mat) {
        int row = mat.length;
        int col = mat[0].length;
        int maxoccurance=0;
        int index=-1;
        for(int i=0;i<row;i++){
            int count=0;
            for(int j=0;j<col;j++){
                if(mat[i][j]==1){
                    count++;
                }
            }
            if(count>maxoccurance){
                maxoccurance=count;
                index=i;
            }


        } 
        return new int[]{index,maxoccurance};
    }
    
}
