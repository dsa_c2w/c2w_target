class Solutuin{
        int pivot(int arr[],int n){
                int arr1[]=new int[n];
                arr1[0]=arr[0];
                for(int i=1;i<n;i++){
                        arr1[i]=arr1[i-1]+arr[i];
                }
                int[] arr2=new int[n];
                arr2[n-1]=arr[n-1];
                for(int i=n-2;i>=0;i--){
                        arr2[i]=arr2[i+1]+arr[i];
                }
                for(int i=0;i<n;i++){
                        if(arr1[i]==arr2[i])
                                return i+1;
                }
                return -1;
        }
}
