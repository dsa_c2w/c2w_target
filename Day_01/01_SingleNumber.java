/*
Description:
Given a non-empty array of integers nums, every element appears
twice except for one. Find that single one.
You must implement a solution with a linear runtime complexity and use
only constant extra space.
Example 1:
Input: nums = [2,2,1]
Output: 1
Example 2:
Input: nums = [4,1,2,1,2]
Output: 4
Example 3:
Input: nums = [1]
Output: 1
Constraints:
1 <= nums.length <= 3 * 10^4
-3 * 104 <= nums[i] <= 3 * 10^4
Each element in the array appears twice except for one element
which appears only once.
*/
import java.util.*;
class Solution{
	public int sNumber(int arr[]){
		int value=0;
		for(int i=0;i<arr.length;i++){
			value=value ^ arr[i];
		}
		return value;
	}
}
class B{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the size of an array");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter the array Elements");
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		Solution ans=new Solution();
		int ret=ans.sNumber(arr);
		System.out.println("The single number in array is" + ret);
	}
}	
