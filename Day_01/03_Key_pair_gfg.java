/*Given an array Arr of N positive integers and another number X. Determine
whether or not there exist two elements in Arr whose sum is exactly X.
Example 1:
Input:
N = 6, X = 16
Arr[] = {1, 4, 45, 6, 10, 8}
Output: Yes
Explanation: Arr[3] + Arr[4] = 6 + 10 = 16
Example 2:
Input:
N = 5, X = 10
Arr[] = {1, 2, 4, 3, 6}
Output: Yes
Explanation: Arr[2] + Arr[4] = 4 + 6 = 10

Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1 ≤ N ≤ 105
1 ≤ Arr[i] ≤ 105
*/
import java.util.*;
class Solution{
	boolean twoElements(int arr[],int x,int n){
		Arrays.sort(arr);
		int i=0;
		int j=n-1;
		while(i<j){
		  if(arr[i]+arr[j]==x)
			return true;
		  else if(arr[i]+arr[j]<x)
			 i++;
		  else
			  j--;
	}
	return false;
	}
}
class client{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		int n=sc.nextInt();
		System.out.println("Enter the elements X");
		int x=sc.nextInt();
		int arr[]=new int[n];
		System.out.println("Enter array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		Solution obj=new Solution();
		boolean ret=obj.twoElements(arr,x,n);
	}
}
		







