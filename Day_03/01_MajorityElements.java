import java.util.*;
class Solution {
    public int major(int[] arr) {

     Arrays.sort(arr);

     int len = arr.length;

     int ans = arr[(len/2)];

     return ans;
    }
}

class Client_Demo{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		int size = sc.nextInt();

		int arr[]= new int[size];

		for(int i = 0; i < size;i++){

			arr[i] = sc.nextInt();
		}
		Solution obj = new Solution();

		int ret = obj.major(arr);

		System.out.println(ret);
	}
}

