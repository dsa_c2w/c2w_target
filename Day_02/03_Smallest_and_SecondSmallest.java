import java.util.*;
class Solution{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the Size of Array");
		int size=sc.nextInt();
		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int small=Integer.MAX_VALUE;
		for(int i=0;i<arr.length;i++){
			if(arr[i]<small){
				small=arr[i];
			}
		}
		System.out.println("Small element is" + small);
		int second_Smallest=Integer.MAX_VALUE;
		for(int i=0;i<arr.length;i++){
			if(arr[i]<second_Smallest && arr[i]> small){
				second_Smallest=arr[i];
			}
		}
		System.out.println("Second Smallest Element is"+ second_Smallest);
	}
}
