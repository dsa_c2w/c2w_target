class Solution {
    public int[] sortedSquares(int[] nums) {

        int index = 0;
        int size = nums.length;
        int arr[] = new int[size];

        for(int i = 0; i < size;i++){

            arr[index] = nums[i] * nums[i];
            index++;
        }

        Arrays.sort(arr);

        return arr;


    }
}


